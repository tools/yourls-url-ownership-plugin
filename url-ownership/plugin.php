<?php
/*
Plugin Name: URL Ownership Plugin
Plugin URI: https://gitlab.collabora.com/tools/yourls-url-ownership-plugin
Description: This plugin records who created a URL
Version: 1.0
Author: Mark Kennedy
Author URI: https://www.collabora.com
*/

// No direct call
if( !defined( 'YOURLS_ABSPATH' ) ) die();

// table to store url owner
if (!defined( 'YOURLS_DB_YOURLS_OWNER' ))
    define( 'YOURLS_DB_YOURLS_OWNER', YOURLS_DB_PREFIX.'url_owner' );

function create_url_ownership_table() {
    /**
     * Initialise the ownership DB
     */
    global $ydb;

    $error_msg = array();
    $success_msg = array();
    $create_tables = array();
    $create_table_count = 0;
    $create_tables[YOURLS_DB_YOURLS_OWNER] =
        'CREATE TABLE IF NOT EXISTS `'.YOURLS_DB_YOURLS_OWNER.'` ('.
        '`keyword` varchar(200) CHARACTER SET utf8mb4 NOT NULL,'.
        '`owner` text CHARACTER SET utf8mb4,'.
        ' PRIMARY KEY  (`keyword`)'.
        ') AUTO_INCREMENT=1 ;';

    foreach ( $create_tables as $table_name => $table_query ) {                 
        $ydb->perform( $table_query );                                          
        $create_success = $ydb->fetchAffected( "SHOW TABLES LIKE '$table_name'" );
        if( $create_success ) {                                                 
           $create_table_count++;                                              
            $success_msg[] = yourls_s( "Table '%s' created.", $table_name );    
        } else {                                                                
            $error_msg[] = yourls_s( "Error creating table '%s'.", $table_name );
        }                                                                       
    }    
    echo implode("<br />", $success_msg);
    echo implode("<br />", $error_msg);
}

yourls_add_action( 'activated_url-ownership/plugin.php', 'create_url_ownership_table' );

function new_owned_link($data) {
    /**
     * Create an ownership record for the posted link
     */
    list($inserted, $url, $keyword, $title, $timestamp, $ip) = $data;

    global $ydb;

    $table = YOURLS_DB_YOURLS_OWNER;
    $sql    = "REPLACE INTO $table VALUES (:keyword, :user)";
    $binds  = array('keyword' => $keyword, 'user' => YOURLS_USER);
    $insert = $ydb->fetchAffected($sql, $binds);
}

yourls_add_action( 'insert_link', 'new_owned_link' );

function uo_add_owner_cells_filter( $cells, $keyword, $url, $title, $ip, $clicks, $timestamp ) {
    /*
     * Filter to create extra cells at end of URL table rows
     * Does lots of queries, but not obvious way round that
     * other than to complete replace the insert logic
     */

    global $ydb;

    $table = YOURLS_DB_YOURLS_OWNER;
    $sql   = "SELECT owner FROM `$table` where `keyword` = :keyword";
    $binds = array('keyword' => $keyword);
    $whoOwns = $ydb->fetchValue($sql, $binds);

    if (!$whoOwns) {
        $whoOwns = 'No Record';
    }

    $cells["owner"] = array(
        'template'      => '%owner%',
        'owner'         => $whoOwns
    );
    return $cells;
}

yourls_add_filter( 'table_add_row_cell_array', 'uo_add_owner_cells_filter' );

function uo_add_owner_header_filter( $cells ) {
    /*
     * Add extra heading to URLs table
     */

    $cells["owner"] = yourls__( 'Owner' );
    return $cells;
}

yourls_add_filter( 'table_head_cells', 'uo_add_owner_header_filter' );

// EOF
