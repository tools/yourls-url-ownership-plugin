# YOURLS URL Ownership Plugin

This plugin simply records which user created which keywork/URL pair. You cannot change the owner for a URL once it is saved.

## Installation

Copy the `url-ownership/` folder to your YOURLS plugin folder, which is typically `var/www/html/user/plugins/`

YOURLS should automatically detect the new plugin and show in the Admin -> Manage Plugins section.

Activating the plugin will create a new database table, so ensure your are connecting to the database using a user with CREATE privileges.

### Manual database creation

Should you wish not to use a highly privileged DB account, then you can create the table yourself:

```
CREATE TABLE IF NOT EXISTS yourls_url_owner (
	`keyword` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
	`owner` text CHARACTER SET utf8mb4,
	 PRIMARY KEY  (`keyword`)
);
```

You may need to change the table name if you are using a custom table prefix.

## Get hacking

The included `docker-compose` config should be sufficient to get started:

```
docker-compose up
```

The folder `url-ownership/` is mounted into the container's YOURLS plugins folder and you can
edit it live.
